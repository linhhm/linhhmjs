/*var dictSource=[];*/
var linhhm = {
    Core: {
        randomString: function(length) {
            var text = "";
            var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            for( var i=0; i < length; i++ )
                text += possible.charAt(Math.floor(Math.random() * possible.length));
            return text;
        },
        upload: function(fd, option) {
            if(fd instanceof HTMLFormElement) {
                fd = new FormData(fd);
            }
            var defaultOption = {
                processData : (fd instanceof FormData) ? false : true,
                contentType: (fd instanceof FormData) ? false/*'multipart/form-data'*/ : 'application/x-www-form-urlencoded; charset=UTF-8',
                forceSync: false,
                dataType: "JSON",
                type: "POST",
                cache: false,
                url: document.URL,
                error: function(e) {
                    console.dir(e);
                },
                process: function(p) {
                    if(p == 100) {
                        $("#process-bar").show().width("100%");
                        window.setTimeout(function(){$("#process-bar").width(0).hide();}, 500);
                    } else {
                        $("#process-bar").show().width(p + "%");
                    }
                }
            }
            if(!option.url) {
                throw new Error('Ajax thieu url');
            }
            var setting = _.extend(defaultOption, option);
            var xhr = $.ajaxSettings.xhr();
            if(setting.process && xhr.upload) {
                xhr.upload.addEventListener('progress', function(event) {
                    var percent = 0;
                    var position = event.loaded || event.position;
                    var total = event.total || e.totalSize;
                    if(event.lengthComputable){
                        percent = Math.ceil(position / total * 100);
                    }

                    if(setting.process) {
                        setting.process.call(this,percent);
                    }
                }, false);
            }
            var uploadObject = new linhhm.Core.Upload(xhr, setting, fd);
            if(!setting.wait) {
                uploadObject.run();
            }
            return uploadObject;
        },
        dauTiengViet: function(input) {
            var coDau = Array("à","á","ạ","ả","ã","â","ầ","ấ","ậ","ẩ","ẫ","ă","ằ","ắ","ặ","ẳ","ẵ","è","é","ẹ","ẻ","ẽ","ê","ề","ế","ệ","ể","ễ","ì","í","ị","ỉ","ĩ","ò","ó","ọ","ỏ","õ","ô","ồ","ố","ộ","ổ","ỗ","ơ","ờ","ớ","ợ","ở","ỡ","ù","ú","ụ","ủ","ũ","ư","ừ","ứ","ự","ử","ữ","ỳ","ý","ỵ","ỷ","ỹ","đ","À","Á","Ạ","Ả","Ã","Â","Ầ","Ấ","Ậ","Ẩ","Ẫ","Ă","Ằ","Ắ","Ặ","Ẳ","Ẵ","È","É","Ẹ","Ẻ","Ẽ","Ê","Ề","Ế","Ệ","Ể","Ễ","Ì","Í","Ị","Ỉ","Ĩ","Ò","Ó","Ọ","Ỏ","Õ","Ô","Ồ","Ố","Ộ","Ổ","Ỗ","Ơ","Ờ","Ớ","Ợ","Ở","Ỡ","Ù","Ú","Ụ","Ủ","Ũ","Ư","Ừ","Ứ","Ự","Ử","Ữ","Ỳ","Ý","Ỵ","Ỷ","Ỹ","Đ");
            var khongDau = Array("a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","e","e","e","e","e","e","e","e","e","e","e","i","i","i","i","i","o","o","o","o","o","o","o","o","o","o","o","o","o","o","o","o","o","u","u","u","u","u","u","u","u","u","u","u","y","y","y","y","y","d","A","A","A","A","A","A","A","A","A","A","A","A","A","A","A","A","A","E","E","E","E","E","E","E","E","E","E","E","I","I","I","I","I","O","O","O","O","O","O","O","O","O","O","O","O","O","O","O","O","O","U","U","U","U","U","U","U","U","U","U","U","Y","Y","Y","Y","Y","D");
            var output = input;

            for(var i = 0; i < coDau.length; i++) {
                output = output.replace(new RegExp(coDau[i], 'g'), khongDau[i]);
            }
            return output;
        },
        stringToAlias: function(input) {
            return window.linhhm.Utilities.dauTiengViet(input).toLowerCase().replace(/[^a-z0-9\-]/i,'').replace(/\-+/,'-');
        },
        convertRedirectForm: function(str) {
            return str.replace(/\-([a-z])/, function(st0,st1) {
                return st1.toUpperCase();
            });
        },
        parseURL: function(url) {
            var parser = document.createElement('a'),
                searchObject = {},
                queries, split, i;
            // Let the browser do the work
            parser.href = url;
            // Convert query string to object
            queries = parser.search.replace(/^\?/, '').split('&');
            for( i = 0; i < queries.length; i++ ) {
                split = queries[i].split('=');
                searchObject[split[0]] = split[1];
            }
            var path = parser.pathname;
            var routerArr = [];
            var moduleList = ["index", "admin"];
            var router = {
                module: 'index',
                controller: 'index',
                action: 'index',
                param: [[], {}, []]
            }
            if(!(/^\/+$/.test(path))) {
                path = path.replace(/^\/+/,"").replace(/\/+$/,"").replace(/\s+/, "").replace(/\/+/,"/");
                routerArr = path.split(/\//);
                var i = 0;


                if(typeof routerArr[i] != "undefined") {
                    if(moduleList.indexOf(routerArr[i]) != -1) {
                        router.module = linhhm.Core.convertRedirectForm(routerArr[i]);
                        delete routerArr[i];
                        i++;
                    }
                    if(router.module == "index") {
                        router.controller = "index";
                        if(typeof routerArr[i] != "undefined") {
                            router.action = linhhm.Core.convertRedirectForm(routerArr[i]);
                            delete routerArr[i];
                        }
                    } else {
                        if(typeof routerArr[i] != "undefined") {
                            router.controller = linhhm.Core.convertRedirectForm(routerArr[i]);
                            delete routerArr[i];
                            i++;
                            if(typeof routerArr[i] != "undefined") {
                                router.action = linhhm.Core.convertRedirectForm(routerArr[i]);
                                delete routerArr[i];
                            }
                        }
                    }
                }
                var tmp = [];
                for(var i = 0; i < routerArr.length; i++) {
                    if(typeof routerArr[i] != "undefined") {
                        router.param[0].push(routerArr[i]);
                        var tmp = routerArr[i].match(/^(.+)\.([^\.]+)$/);

                        if(tmp) {
                            router.param[1][tmp[1]] = tmp[2];
                        } else {
                            router.param[2].push(routerArr[i]);
                        }
                    }
                }
            }
            return {
                protocol: parser.protocol,
                host: parser.host,
                hostname: parser.hostname,
                port: parser.port,
                pathname: parser.pathname,
                search: parser.search,
                searchObject: searchObject,
                hash: parser.hash,
                router: router
            }
        },
        inherit: function(p) {
            if (p == null) throw TypeError(); // p must be a non-null object
            if (Object.create) // If Object.create() is defined...
                return Object.create(p); // then just use it.
            var t = typeof p; // Otherwise do some more type checking
            if (t !== "object" && t !== "function") throw TypeError();
            function f() {}; // Define a dummy constructor function.
            f.prototype = p; // Set its prototype property to p.
            return new f(); // Use f() to create an "heir" of p.
        },
        eventAbstract: function() {
            throw new Error("abstract class only");
        },
        stringToDom: function(s) {
            return $.parseHTML(s);
        },
        rgbaTohex: function(rgb) {
            var color = (rgb.match(/^rgba?[\s+]?\([\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?/i)) ? rgb.match(/^rgba?[\s+]?\([\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?/i) : rgb.match(/^rgb?[\s+]?\([\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?/i);
            return (color && color.length === 4) ? "#" +
            ("0" + parseInt(color[1],10).toString(16)).slice(-2) +
            ("0" + parseInt(color[2],10).toString(16)).slice(-2) +
            ("0" + parseInt(color[3],10).toString(16)).slice(-2) : '';
        }
    },
    Php: {
        sprintf: function() {
            var regex = /%%|%(\d+\$)?([\-+\'#0 ]*)(\*\d+\$|\*|\d+)?(?:\.(\*\d+\$|\*|\d+))?([scboxXuideEfFgG])/g;
            var a = arguments;
            var i = 0;
            var format = a[i++];
            var pad = function(str, len, chr, leftJustify) {
                if (!chr) {
                    chr = ' ';
                }
                var padding = (str.length >= len) ? '' : new Array(1 + len - str.length >>> 0)
                    .join(chr);
                return leftJustify ? str + padding : padding + str;
            };
            var justify = function(value, prefix, leftJustify, minWidth, zeroPad, customPadChar) {
                var diff = minWidth - value.length;
                if (diff > 0) {
                    if (leftJustify || !zeroPad) {
                        value = pad(value, minWidth, customPadChar, leftJustify);
                    } else {
                        value = value.slice(0, prefix.length) + pad('', diff, '0', true) + value.slice(prefix.length);
                    }
                }
                return value;
            };
            var formatBaseX = function(value, base, prefix, leftJustify, minWidth, precision, zeroPad) {
                // Note: casts negative numbers to positive ones
                var number = value >>> 0;
                prefix = (prefix && number && {
                        '2'  : '0b',
                        '8'  : '0',
                        '16' : '0x'
                    }[base]) || '';
                value = prefix + pad(number.toString(base), precision || 0, '0', false);
                return justify(value, prefix, leftJustify, minWidth, zeroPad);
            };

            // formatString()
            var formatString = function(value, leftJustify, minWidth, precision, zeroPad, customPadChar) {
                if (precision !== null && precision !== undefined) {
                    value = value.slice(0, precision);
                }
                return justify(value, '', leftJustify, minWidth, zeroPad, customPadChar);
            };

            // doFormat()
            var doFormat = function(substring, valueIndex, flags, minWidth, precision, type) {
                var number, prefix, method, textTransform, value;

                if (substring === '%%') {
                    return '%';
                }

                // parse flags
                var leftJustify = false;
                var positivePrefix = '';
                var zeroPad = false;
                var prefixBaseX = false;
                var customPadChar = ' ';
                var flagsl = flags.length;
                var j;
                for (j = 0; flags && j < flagsl; j++) {
                    switch (flags.charAt(j)) {
                        case ' ':
                            positivePrefix = ' ';
                            break;
                        case '+':
                            positivePrefix = '+';
                            break;
                        case '-':
                            leftJustify = true;
                            break;
                        case "'":
                            customPadChar = flags.charAt(j + 1);
                            break;
                        case '0':
                            zeroPad = true;
                            customPadChar = '0';
                            break;
                        case '#':
                            prefixBaseX = true;
                            break;
                    }
                }

                // parameters may be null, undefined, empty-string or real valued
                // we want to ignore null, undefined and empty-string values
                if (!minWidth) {
                    minWidth = 0;
                } else if (minWidth === '*') {
                    minWidth = +a[i++];
                } else if (minWidth.charAt(0) === '*') {
                    minWidth = +a[minWidth.slice(1, -1)];
                } else {
                    minWidth = +minWidth;
                }

                // Note: undocumented perl feature:
                if (minWidth < 0) {
                    minWidth = -minWidth;
                    leftJustify = true;
                }

                if (!isFinite(minWidth)) {
                    throw new Error('sprintf: (minimum-)width must be finite');
                }

                if (!precision) {
                    precision = 'fFeE'.indexOf(type) > -1 ? 6 : (type === 'd') ? 0 : undefined;
                } else if (precision === '*') {
                    precision = +a[i++];
                } else if (precision.charAt(0) === '*') {
                    precision = +a[precision.slice(1, -1)];
                } else {
                    precision = +precision;
                }

                // grab value using valueIndex if required?
                value = valueIndex ? a[valueIndex.slice(0, -1)] : a[i++];

                switch (type) {
                    case 's':
                        return formatString(String(value), leftJustify, minWidth, precision, zeroPad, customPadChar);
                    case 'c':
                        return formatString(String.fromCharCode(+value), leftJustify, minWidth, precision, zeroPad);
                    case 'b':
                        return formatBaseX(value, 2, prefixBaseX, leftJustify, minWidth, precision, zeroPad);
                    case 'o':
                        return formatBaseX(value, 8, prefixBaseX, leftJustify, minWidth, precision, zeroPad);
                    case 'x':
                        return formatBaseX(value, 16, prefixBaseX, leftJustify, minWidth, precision, zeroPad);
                    case 'X':
                        return formatBaseX(value, 16, prefixBaseX, leftJustify, minWidth, precision, zeroPad)
                            .toUpperCase();
                    case 'u':
                        return formatBaseX(value, 10, prefixBaseX, leftJustify, minWidth, precision, zeroPad);
                    case 'i':
                    case 'd':
                        number = +value || 0;
                        // Plain Math.round doesn't just truncate
                        number = Math.round(number - number % 1);
                        prefix = number < 0 ? '-' : positivePrefix;
                        value = prefix + pad(String(Math.abs(number)), precision, '0', false);
                        return justify(value, prefix, leftJustify, minWidth, zeroPad);
                    case 'e':
                    case 'E':
                    case 'f': // Should handle locales (as per setlocale)
                    case 'F':
                    case 'g':
                    case 'G':
                        number = +value;
                        prefix = number < 0 ? '-' : positivePrefix;
                        method = ['toExponential', 'toFixed', 'toPrecision']['efg'.indexOf(type.toLowerCase())];
                        textTransform = ['toString', 'toUpperCase']['eEfFgG'.indexOf(type) % 2];
                        value = prefix + Math.abs(number)[method](precision);
                        return justify(value, prefix, leftJustify, minWidth, zeroPad)[textTransform]();
                    default:
                        return substring;
                }
            };

            return format.replace(regex, doFormat);
        },
        in_array: function(needle, haystack, argStrict) {
            var key = '',
                strict = !! argStrict;
            if (strict) {
                for (key in haystack) {
                    if (haystack[key] === needle) {
                        return true;
                    }
                }
            } else {
                for (key in haystack) {
                    if (haystack[key] == needle) {
                        return true;
                    }
                }
            }

            return false;
        },
        number_format: function(number, decimals, dec_point, thousands_sep) {
            number = (number + '')
                .replace(/[^0-9+\-Ee.]/g, '');
            var n = !isFinite(+number) ? 0 : +number,
                prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
                sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
                dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
                s = '',
                toFixedFix = function(n, prec) {
                    var k = Math.pow(10, prec);
                    return '' + (Math.round(n * k) / k)
                            .toFixed(prec);
                };
            s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
                .split('.');
            if (s[0].length > 3) {
                s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
            }
            if ((s[1] || '')
                    .length < prec) {
                s[1] = s[1] || '';
                s[1] += new Array(prec - s[1].length + 1)
                    .join('0');
            }
            return s.join(dec);
        }
    },
    notification: window.Notification || window.mozNotification || window.webkitNotification
};
linhhm.Core.Upload = function(xhr, setting, fd) {
    this.xhr = xhr;
    this.setting = setting;
    this.fd = fd;
};
linhhm.Core.Upload.prototype = linhhm.Core.inherit(Backbone.Events);
_.extend(linhhm.Core.Upload.prototype, {
    getXhr: function() {
        return this.xhr;
    },
    run: function() {
        var t = this;
        var setting = t.setting;
        var fd = t.fd;
        var xhr = t.xhr;
        $.ajax({
            url: setting.url,
            type: setting.type,
            dataType: setting.dataType,
            data: fd,
            cache: setting.cache,
            contentType: setting.contentType,
            processData: setting.processData,
            forceSync: setting.forceSync,
            xhr: function(){
                return xhr;
            },
            success: function (data, status, xhr){
                console.log(setting.url);
                if(data.errorLogin) {
                    window.alert("Hết hạn đăng nhập, bạn hãy đăng nhập lại, có thể chọn nhớ đăng nhập để lưu trạng thái")
                    window.location = "/admin/account/sign-in";
                } else if(setting.success) {
                    if(setting.linhhmStandard) {
                        if(data.status) {
                            if(typeof data.data != "underfined") {
                                arguments[0] = data.data;
                            } else {
                                arguments[0] = [];
                            }
                            setting.success.apply(this,arguments);
                        } else {
                            setting.error.apply(this,arguments);
                        }
                    } else {
                        setting.success.apply(this,arguments);
                    }
                }
                t.trigger('success', data, status, xhr);
            },
            error: function (xhr, status, error){
                console.log(setting.url);
                if(setting.error) {
                    setting.error.apply(this,arguments);
                }
                t.trigger('error', xhr, status, error);
            },
            complete: function(xhr, status){
                if(setting.complete) {
                    setting.complete.apply(this, arguments);
                }
                t.trigger('complete', xhr, status);
            }
        });
    }
});
_.extend(linhhm.Core.eventAbstract.prototype, {
    eventInit: function() {
        var t = this;
        t._eventData = [];
    },
    on: function() {
        var arg = arguments;
        var t = this;
        if(arg.length < 2) {
            throw new Error("su kien can co ten");
        } else {
            var name = arguments[0];
            var hander = arguments[1];
            if(!_.isString(name)) {
                throw new Error("ten su kien phai la 1 chuoi");
            } else if(!name.match(/^[a-z\s0-9]+$/i)) {
                throw new Error("ten su kien tu a-z 0-9");
            }
            if(!_.isFunction(hander)) {
                throw new Error("trinh xu ly phai la mot funciton");
            }

            if(name.match(/\s+/)) {
                name = name.split(/\s+/);
            }

            if(_.isArray(name)) {
                for(var i = 0; i < name.length; i++) {
                    t._eventData.push({
                        name: name[i],
                        hander: hander
                    });
                }
            } else {
                t._eventData.push({
                    name: name,
                    hander: hander
                });
            }
        }
    },
    once: function() {
        var arg = arguments;
        var t = this;
        if(arg.length < 2) {
            throw new Error("su kien can co ten");
        } else {
            var name = arguments[0];
            var hander = arguments[1];
            if(!_.isString(name)) {
                throw new Error("ten su kien phai la 1 chuoi");
            } else if(!name.match(/^[a-z\s0-9]+$/i)) {
                throw new Error("ten su kien tu a-z 0-9");
            }
            if(!_.isFunction(hander)) {
                throw new Error("trinh xu ly phai la mot funciton");
            }

            if(name.match(/\s+/)) {
                name = name.split(/\s+/);
            }

            if(_.isArray(name)) {
                for(var i = 0; i < name.length; i++) {
                    t._eventData.push({
                        name: name[i],
                        hander: hander,
                        once: true
                    });
                }
            } else {
                t._eventData.push({
                    name: name,
                    hander: hander,
                    once: true
                });
            }
        }
    },
    off: function() {
        var arg = arguments;
        var t = this;
        var name;
        var hander = undefined;
        if(arg.length < 1) {
            throw new Error("Cần có ít nhất một tham số");
        } else if(arg.length == 1) {
            name = arg[0];
        } else {
            name = arg[0];
            hander = arg[1];
        }
        if(!_.isString(name)) {
            throw new Error("Tên sự kiện là một chuỗi");
        } else if(!name.match(/^[a-z0-9]+$/i)) {
            throw new Error("ten su kien tu a-z 0-9");
        }

        if(!(_.isFunction(hander) || _.isUndefined(hander))) {
            throw new Error("trình xử lý sự kiện phải là một function");
        }
        if(_.isFunction(hander)) {
            for(var i = 0; i < t._eventData.length; i++) {
                if(t._eventData[i]["name"] == name && hander == t._eventData[i]["hander"]) {
                    delete t._eventData[i];
                }
            }
        } else {
            for(var i = 0; i < t._eventData.length; i++) {
                if(t._eventData[i]["name"] == name) {
                    delete t._eventData[i];
                }
            }

        }
        t._eventData =_.compact(t._eventData);

    },
    hasHander: function() {
        var arg = arguments;
        var t = this;
        var name;
        var hander = undefined;
        var flag = false;
        if(arg.length < 1) {
            throw new Error("Cần có ít nhất một tham số");
        } else if(arg.length == 1) {
            name = arg[0];
        } else {
            name = arg[0];
            hander = arg[1];
        }
        if(!_.isString(name)) {
            throw new Error("Tên sự kiện là một chuỗi");
        } else if(!name.match(/^[a-z0-9]+$/i)) {
            throw new Error("ten su kien tu a-z 0-9");
        }

        if(!(_.isFunction(hander) || _.isUndefined(hander))) {
            throw new Error("trình xử lý sự kiện phải là một function");
        }

        if(_.isFunction(hander)) {
            for(var i = 0; i < t._eventData.length; i++) {
                if(t._eventData[i]["name"] == name && hander == t._eventData[i]["hander"]) {
                    flag = true;
                    break;
                }
            }
        } else {
            for(var i = 0; i < t._eventData.length; i++) {
                if(t._eventData[i]["name"] == name) {
                    flag = true;
                    break;
                }
            }

        }
        return flag;

    },
    fire: function() {
        var arg = arguments;
        var t = this;
        var name;
        var param = [];
        if(arg.length < 1) {
            throw new Error("Cần có ít nhất một tham số");
        } else {
            name = arg[0];
        }
        if(!_.isString(name)) {
            throw new Error("Tên sự kiện là một chuỗi");
        } else if(!name.match(/^[a-z0-9A-Z]+$/i)) {
            throw new Error("ten su kien tu a-z 0-9");
        }
        for(var i = 1; i < arg.length; i++) {
            param.push(arg[i]);
        }
        
        var once = false;
        for(var i = 0; i < t._eventData.length; i++) {
            if(t._eventData[i]["name"] == name) {
                t._eventData[i]["hander"].apply(t, param);
                if(t._eventData[i]["once"]) {
                    delete t._eventData[i];
                    once = true;
                }
            }
        }
        if(once) {
            t._eventData = _.compact(t._eventData);
        }
    }
});
linhhm.Data = function() {
    this._data = [];
}
linhhm.Data.data = {};

/*linhhm.Data.prototype = linhhm.Core.inherit(linhhm.Core.eventAbstract.prototype);*/
linhhm.Data.prototype = linhhm.Core.inherit(Backbone.Events);

_.extend(linhhm.Data.prototype, {
    e: function() {
        var t = this;
        var setFlag = false;
        var key;
        var value;
        if(arguments.length < 1) {
            throw new Error("thieu tham so");
        } else if(arguments.length == 1) {
            key = arguments[0];
        } else {
            key = arguments[0];
            value = arguments[1];
            setFlag = true;
        }
        //set
        var flagExists = false;
        if(setFlag) {
            for(var i = 0; i < t._data.length; i++) {
                if(t._data[i] && t._data[i]["key"] == key) {
                    flagExists = true;
                    t._data[i]["value"] = value;
                    break;
                }
            }
            if(flagExists) {
                t.trigger("updateElement", key, value);
            } else {
                t._data.push({
                    key: key,
                    value: value
                });
                t.trigger("newElement", key, value);
            }
            t.trigger("setElement", key, value);
            return t;
        } else { //get
            for(var i = 0; i < t._data.length; i++) {
                if(t._data[i] && t._data[i]["key"] == key) {
                    flagExists = true;
                    t.trigger("getElement", key, t._data[i]["value"]);
                    return t._data[i]["value"];
                }
            }
            return;
        }
    },
    remove: function(key) {
        var t = this;
        var flagRemove = false;
        var value;
        for(var i = 0; i < t._data.length; i++) {
            if(t._data[i] && t._data[i]["key"] == key) {
                value = t._data[i]["value"];
                delete t._data[i];
                flagRemove = true;
                break;
            }
        }
        if(flagRemove) {
            t.trigger("removeElement", key, value);
            t._data = _.compact(t._data);
        }
        return this;
    },
    exists: function(key) {
        var t = this;
        var flag = false;
        for(var i = 0; i < t._data.length; i++) {
            if(t._data[i].key == key) {
                flag = true;
                break;
            }
        }
        return flag;
    },
    each: function(f) {
        var t = this;
        if(_.isFunction(f)) {
            for(var i = 0; i < t._data.length; i++) {
                f.call(t, t._data[i]["key"], t._data[i]["value"]);
            }
        } else {
            throw new Error("fai la function");
        }
    },
    search: function(key_word) {
        var t = this;
        var result = [];
        for(var i = 0; i < t._data.length; i++) {
            if(_.isString(t._data[i].key) && t._data[i].key.indexOf(key_word) != -1) {
                result.push(t._data[i]);
            }
        }
        return result;
    },
    searchValue: function(key_word) {
        var t = this;
        var result = [];
        for(var i = 0; i < t._data.length; i++) {
            if(_.isString(t._data[i].value) && t._data[i].value.indexOf(key_word) != -1) {
                result.push(t._data[i]);
            }
        }
        return result;
    },
    searchCallback: function(key_word, callback) {
        var t = this;
        var result = [];
        for(var i = 0; i < t._data.length; i++) {
            if(callback(key_word, t._data[i])) {
                result.push(t._data[i]);
            }
        }
        return result;
    }
});

linhhm.data = function(namespace) {
    if(!_.isString(namespace)) {
        throw new Error("namespace phai la 1 chuoi");
    } else if(!namespace.match(/^[a-z0-9]+$/i)) {
        throw new Error("namespace chỉ cứa a-z 0-9");
    }
    if(!(linhhm.Data.data[namespace] instanceof linhhm.Data)) {
        linhhm.Data.data[namespace] = new linhhm.Data();
    }
    return linhhm.Data.data[namespace];
}


// A function handler
linhhm.UI = {
    notify: function(titleText, bodyText, icon, callback) {
        if(linhhm.notification)
            linhhm.notification.requestPermission(function(permission){});
        if ('undefined' === typeof linhhm.notification)
            return false;       //Not supported....
        var noty = new linhhm.notification(
            titleText, {
                body: bodyText,
                dir: 'ltr', // or ltr, rtl
                lang: 'vi', //lang used within the notification.
                tag: 'notificationPopup', //An element ID to get/set the content
                icon: icon || '/public/images/logo/logo.png' //The URL of an image to be used as an icon
            }
        );
        if(callback) {
            if(callback.click)
                noty.onclick = callback.click;
            if(callback.error)
                noty.onerror = callback.error;
            if(callback.show)
                noty.onshow = callback.show;
            if(callback.close)
                noty.onclose = callback.close;
        }
        window.setTimeout(function() {
            noty.close();
        }, 5000);
        return true;
    },
    confirm: function (options) {
        if(typeof options == 'string') {options = {messenger:options};}
        if(_.isArray(options.messenger)) {
            var messenger = '<ul>';
            for(var i = 0; i < options.messenger.length; i++) {
                messenger += '<li>'+options.messenger[i]+'</li>';
            }
            messenger += '</ul>';
            options.messenger = messenger;
        }

        var confirmBox = $('<div class="system-confirm z-depth-4"><div class="notice-wrap"><h4 class="title">title</h4><div class="notice-content"></div></div><div class="notice-action"><button class="ok waves-effect waves-teal btn-flat white">OK</button><button class="cancel waves-effect waves-red btn-flat white">Cancel</button></div></div>').appendTo('body');
        var overlay = $('<div class="system-overlay"></div>').addClass("active").appendTo("body");

        overlay.on("click", function(){
            confirmBox.remove();
            overlay.remove();
        });
        confirmBox.find(".notice-action > button.cancel").on('click', function(){
            confirmBox.remove();
            overlay.remove();
            if(options && options.cancelCallback) {
                options.cancelCallback();
            }
        });
        confirmBox.find(".notice-action > button.ok").on('click', function(){
            confirmBox.remove();
            overlay.remove();
            if(options && options.okCallback) {
                options.okCallback();
            }
        });
        if(options.title) {
            confirmBox.find(".notice-wrap > h4.title").html(options.title);
        }
        if(options.messenger) {
            confirmBox.find(".notice-wrap > .notice-content").html(options.messenger);
        }
    },
    notice: function(options){
        if(typeof options == 'string') {options = {messenger:options};}
        if(_.isArray(options.messenger)) {
            var messenger = '<ul>';
            for(var i = 0; i < options.messenger.length; i++) {
                messenger += '<li>'+options.messenger[i]+'</li>';
            }
            messenger += '</ul>';
            options.messenger = messenger;
        }

        var noticeBox = $('<div class="system-notice" class="z-depth-4"><div class="notice-wrap"><h4 class="title">title</h4><div class="notice-content"></div></div><div class="notice-action"><button class="waves-effect waves-teal btn-flat white">OK</button></div></div>').appendTo('body');
        var overlay = $('<div class="system-overlay"></div>').appendTo("body");

        overlay.on("click", function(){
            noticeBox.remove();
            overlay.remove();
            if(options && options.callback) {
                options.callback();
            }
        });
        noticeBox.find(".notice-action > button").off("click").on('click', function(){
            noticeBox.remove();
            overlay.remove();
            if(options && options.callback) {
                options.callback();
            }
        });
        if(options.title) {
            noticeBox.find(".notice-wrap > h4.title").html(options.title);
        }
        if(options.messenger) {
            noticeBox.find(".notice-wrap > .notice-content").html(options.messenger);
        }

    },
    template: function(selector) {
        return _.template($(selector).html());
    },
    phanTrang: function(link, cPage, tPage) {
        var html = '';
        if(tPage > 1) {
            var start = (cPage >= 5) ? (cPage - 4) : 1;
            var end = (tPage - cPage > 5) ? (cPage + 4) : tPage;

            html += '<ul class="pagination">';

            for(var i = start; i < cPage; i++) {
                html += '<li class="waves-effect"><a data-page="'+i+'" href="'+ linhhm.Php.sprintf(link, i)+ '">'+(i)+'</a></li>';
            }

            html += '<li class="active"><a href="javascript:void(0);">'+cPage+'</a></li>';
            for(i = cPage + 1; i <= end; i++) {
                html += '<li class="waves-effect"><a data-page="'+i+'" href="'+linhhm.Php.sprintf(link, i)+'">'+(i)+'</a></li>';
            }

            html += '</ul>';
        }
        return html;

    }
}
linhhm.Utilities = {
    getFormData: function(form, options) {
        return $(form).serialize();
    },
    getFormArray: function(form) {
         var tmp = $(form).serializeArray();
         var data = {};
         for(var i = 0; i< tmp.length; i++) {
             data[tmp[i]['name']] = tmp[i]['value'];
         }
         return data;

    }
}
linhhm.Image = function() {
    var t = this;
    t.id = linhhm.Core.randomString(5);

    this.$b = $('<div class="lhmImage-box">' +
        '<div class="row lhmImage-toolbar">' +
        '<ul class="col s12">' +
        '<li><span class="fa fa-folder"></span> new</li>' +
        '<li><input multiple id="'+ t.id+'-upload" type="file" style="display: none"><label for="'+ t.id+'-upload" class="fa fa-image"> Upload</label></li>' +
        '</ul></div><div class="row"><div class="col s4 lhmImage-folder"><ul></ul></div><div class="col s8 lhmImage-list"><ul></ul></div></div></div>')
    this.b = this.$b[0];
    this.f = this.$b.find('.lhmImage-folder > ul');
    this.l = this.$b.find('.lhmImage-list > ul');
    this.ul = this.$b.find('.lhmImage-toolbar input')
    t.namespace = 'images';

    document.body.appendChild(this.b);
    this.imageD = new linhhm.ImageDirectory(this, null, 'images', 'images', this.f);
    this.imageD.showImage();
    t.currentDirObj = this.imageD;
    this.ul.on('change', function() {
        var file = this.files;
        if(file.length) {
            var form = new FormData();
            var count = 0;
            form.append('path', t.namespace);
            for(var i = 0; i < file.length; i++) {
                if(file[i].type == 'image/jpeg' || file[i].type == 'image/png') {
                    count++;
                    form.append('image[]', file[i]);
                }
            }
            if(count) {
                linhhm.Core.upload(form, {
                    url: '/admin/content-edit/ajax-upload-image',
                    success: function(data) {
                        if(data.status) {
                            t.currentDirObj.loadChild(t.currentDirObj.showImage);
                        }
                    }
                })
            }
        }
    });
    this.clickOut = function(e) {
        var target = e.target;
        var inNode = false;

        while(target && target != document.body) {
            if(target == t.b) {
                inNode = true;
                break;
            }
            target = target.parentNode;
        }

        if(!inNode) {
            t.closeDialog();
        }
    }
}
linhhm.Image.prototype = linhhm.Core.inherit(linhhm.Core.eventAbstract.prototype);
_.extend(linhhm.Image.prototype, {
    loadDirectory: function() {
        this.imageD.loadChild();
    },
    setCallback: function(callback, context) {
        this.callback = callback;
        this.callbackContext = context;
    },
    openDialog: function() {
        var t = this;
        window.setTimeout(function() {
            t.$b.addClass('active');
            $(document).on('click', t.clickOut);
        },200);
    },
    closeDialog: function() {
        this.$b.removeClass('active');
        $(document).off('click', this.clickOut);
    }
})
linhhm.ImageDirectory = function(imgObj, parent, namespace, name, wrap) {
    this.imgObj = imgObj;
    this.parent = parent;
    this.wrap = wrap;
    this.name = name;
    this.namespace = namespace;
    this.childDir = [];
    this.childImg = [];
    this.loaded = false;
    this.buildUI();
}
linhhm.ImageDirectory.prototype = linhhm.Core.inherit(linhhm.Core.eventAbstract.prototype);
_.extend(linhhm.ImageDirectory.prototype, {
    loadChild: function(callback) {
        var t = this;
        this.childImg.length = 0;
        linhhm.Core.upload({directory: t.namespace}, {
            url: "/admin/content-edit/ajax-get-directory-list",
            success: function(data) {
                if(data.status) {
                    t.loaded = true;
                    for(var i = 0; i < data.data.dir.length; i++) {
                        var newNamespace = t.namespace + '/' +data.data.dir[i];
                        var flagHas = false;
                        for(var y = 0; y < t.childDir.length; y++) {
                            if(t.childDir[y].namespace == newNamespace) {
                                flagHas == true;
                                break;
                            }
                        }
                        if(!flagHas) {
                            t.childDir.push(new linhhm.ImageDirectory(t.imgObj, t, newNamespace,data.data.dir[i], t.f));
                        }
                    }
                    for(var i = 0; i < data.data.img.length; i++) {
                        t.childImg.push(data.data.img[i]);
                    }
                    if(callback)
                        callback.call(t);
                }
            }
        });
    },
    buildUI: function() {
        var t = this;
        t.dom = $('<li><div class="name"></div><div class="ex"><span class="fa fa-plus"></span><span class="fa fa-minus"></span></div><ul></ul></li>');
        t.dom.find('> .name').html(t.name);
        t.dom.find('> .ex').on('click', function(){
            t.ex();
        });
        t.dom.find('> .name').on('click', function(){
            t.showImage();
        });
        t.wrap.append(t.dom);
        t.f = t.dom.find('> ul');
    },
    showImage: function() {
        var t= this;
        t.imgObj.namespace = t.namespace;
        t.imgObj.currentDirObj = t;
        if(t.loaded) {
            t.imgObj.l.empty();
            for(var i = 0; i < t.childImg.length; i++) {
                var li = $('<li><div class="img"><img src="" alt=""></div><div class="name"></div></li>');
                li.find('img').attr('src', '/public/' + t.namespace + '/' + t.childImg[i]);
                li.find('.name').text(t.childImg[i]);
                li.on('dblclick', function() {
                    var src = $(this).find('img').attr('src');
                    t.imgObj.closeDialog();
                    if(t.imgObj.callback) {
                        if(t.imgObj.callbackContext)
                            t.imgObj.callback.call(t.imgObj.callbackContext, src);
                        else
                            t.imgObj.callback(src);
                    }
                })
                t.imgObj.l.append(li);
            }
        } else {
            t.loadChild(t.showImage);
        }

    },
    ex: function() {
        var t= this;
        if(t.dom.hasClass('ex')) {
            t.dom.removeClass('ex');
        } else {
            t.dom.addClass('ex');
            if(!t.loaded) {
                t.loadChild(t.ex);
            }
        }
    }
});



Backbone.sync = function(method, model, options) {
    var xhr;
    var uploadConfig = {};
    var data = {};
    switch(method) {
        case 'create': {
            uploadConfig.url = model.urlNew;
        }
            break;
        case 'update': {
            uploadConfig.url = model.urlEdit;
            if(!model._cChanging)
                flag = false;
        }
            break;
        case 'delete': {
            uploadConfig.url = model.urlDelete;
        }
            break;
        case 'read': {
            uploadConfig.url = model.urlRead;
        }
            break;
        default:
            throw new Error('unknow method');
    }

    if(options) {
        if(options.success) {
            uploadConfig.success = function() {
                console.dir(arguments);
                options.success.apply(model,arguments);
            }
        }

        if(options.error) {
            uploadConfig.error = function(){
                console.dir(arguments);
                options.error.apply(model,arguments);
            }
        }
    }
    if(model instanceof Backbone.Collection) {
        if(options.data) {
            data = options.data;
        }
    } else if(model instanceof Backbone.Model) {
        data = model.attributes;
    }
    uploadConfig.linhhmStandard = true;
    uploadObject = linhhm.Core.upload(data, uploadConfig);
    model.trigger('request', model, xhr, options);
    return uploadObject.getXhr();
}